import numpy as np
print("Podaj poniższe wartości")
szerokosc = int(input("Podaj szerokość: "))
dlugosc = int(input("Podaj dlugość: "))
ilosc_punktow = int(input("Podaj liczbę punktów: "))
def generuj_chmure_punktow(ilosc_punktow, szerokosc, dlugosc):
    """
    Generuje chmurę punktów w przestrzeni 3D na płaskiej powierzchni.
    Parametry:
    - ilosc_punktow: Liczba punktów do wygenerowania.
    - szerokosc: Szerokość obszaru generacji punktów.
    - dlugosc: Długość obszaru generacji punktów.

    Zwraca:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    """
   
    x =np.random.uniform(0, szerokosc, ilosc_punktow)
    y = np.random.uniform(0, dlugosc, ilosc_punktow)
    z =np.zeros(ilosc_punktow)  # Wszystkie punkty mają współrzędną z równą zero.

    chmura_punktow = list(zip(x, y, z))
    return chmura_punktow

def wyswietl_chmure_punktow(chmura_punktow):
    """
    Wyświetla chmurę punktów w wierszu.

    Parametry:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    """
    for punkt in chmura_punktow:
        print(f'({punkt[0]}, {punkt[1]}, {punkt[2]})', end=' ')
    print()  # Nowa linia po wyświetleniu wszystkich punktów.

def zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku):
    """
    Zapisuje listę punktów do pliku w formacie XYZ.

    Parametry:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    - nazwa_pliku: Nazwa pliku do zapisu w formacie "nazwa.xyz".
    """
    with open(nazwa_pliku, 'w') as plik_xyz:
        for punkt in chmura_punktow:
            x, y, z = punkt
            plik_xyz.write(f'{x} {y} {z}\n')

nazwa_pliku = 'punkty.xyz'

chmura_punktow = generuj_chmure_punktow(ilosc_punktow, szerokosc, dlugosc)

print("Chmura punktów:")
wyswietl_chmure_punktow(chmura_punktow)

zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku)
print(f'Zapisano chmurę punktów do pliku {nazwa_pliku}')
