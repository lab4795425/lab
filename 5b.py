import numpy as np
print("Podaj poniższe wartości")
szerokosc = int(input("Podaj szerokość: "))
wysokosc = int(input("Podaj wysokosc: "))
ilosc_punktow = int(input("Podaj liczbę punktów: "))

def generuj_chmure_punktow(ilosc_punktow, szerokosc, wysokosc):

    x = np.random.uniform(0, szerokosc, ilosc_punktow)
    y = np.random.uniform(0, wysokosc, ilosc_punktow)
    z = np.zeros(ilosc_punktow)  # Wszystkie punkty mają współrzędną z równą zero.

    chmura_punktow = list(zip(x, y, z))
    return chmura_punktow

def wyswietl_chmure_punktow(chmura_punktow):

    for punkt in chmura_punktow:
        print(f'({punkt[0]}, {punkt[1]}, {punkt[2]})', end=' ')
    print()  # Nowa linia po wyświetleniu wszystkich punktów.

def zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku):

    with open(nazwa_pliku, 'w') as plik_xyz:
        for punkt in chmura_punktow:
            x, y, z = punkt
            plik_xyz.write(f'{x} {y} {z}\n')


nazwa_pliku = 'punkty1.xyz'

chmura_punktow = generuj_chmure_punktow(ilosc_punktow, szerokosc, wysokosc)

print("Chmura punktów:")
wyswietl_chmure_punktow(chmura_punktow)

zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku)
print(f'Zapisano chmurę punktów do pliku {nazwa_pliku}')