import numpy as np
print("Podaj poniższe wartości")
promien = int(input("Podaj promień: "))
wysokosc = int(input("Podaj wysokość: "))
ilosc_punktow = int(input("Podaj liczbę punktów: "))
def generuj_chmure_punktow_na_cylindrze(ilosc_punktow, promien, wysokosc):
    """
    Generuje chmurę punktów na powierzchni cylindrycznej.
    Parametry:
    - ilosc_punktow: Liczba punktów do wygenerowania.
    - promien: Promień cylindra.
    - wysokosc: Wysokość cylindra.
    Zwraca:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    """
    theta = np.random.uniform(0, 2 * np.pi, ilosc_punktow)
    z = np.random.uniform(0, wysokosc, ilosc_punktow)
    x = promien * np.cos(theta)
    y = promien * np.sin(theta)

    chmura_punktow = list(zip(x, y, z))
    return chmura_punktow

def wyswietl_chmure_punktow(chmura_punktow):
    """
    Wyświetla chmurę punktów w wierszu.
    Parametry:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    """
    for punkt in chmura_punktow:
        print(f'({punkt[0]}, {punkt[1]}, {punkt[2]})', end=' ')
    print()  # Nowa linia po wyświetleniu wszystkich punktów.

def zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku):
    """
    Zapisuje listę punktów do pliku w formacie XYZ.
    Parametry:
    - chmura_punktow: Lista punktów w formie [(x1, y1, z1), (x2, y2, z2), ...].
    - nazwa_pliku: Nazwa pliku do zapisu w formacie "nazwa.xyz".
    """
    with open(nazwa_pliku, 'w') as plik_xyz:
        for punkt in chmura_punktow:
            x, y, z = punkt
            plik_xyz.write(f'{x} {y} {z}\n')

nazwa_pliku = 'punkty2.xyz'

chmura_punktow = generuj_chmure_punktow_na_cylindrze(ilosc_punktow, promien, wysokosc)

print("Chmura punktów:")
wyswietl_chmure_punktow(chmura_punktow)

zapisz_do_pliku_xyz(chmura_punktow, nazwa_pliku)
print(f'Zapisano chmurę punktów do pliku {nazwa_pliku}')
